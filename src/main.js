import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

Vue.config.productionTip = false;

// Vue.directive("cypress", {
//   inserted: function(el, binding, vnode) {
//     const { value, arg } = binding;
//     el;
//     console.log(vnode.context);
//     console.log(el);
//     const war = "wartosc";
//     el.setAttribute("data-cy", war);
//   },
// });

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");

//czyli mielibyśmy np: onboarding.login-form.login-btn

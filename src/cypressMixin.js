export const cypressMixin = {
  directives: {
    cypress: {
      inserted: function(el, binding, vnode) {
        // const { value, arg } = binding;
        // el;
        console.log(vnode);
        //  console.log(el);

        const preComponentName = vnode.tag.split("-");
        const componentName = preComponentName[preComponentName.length - 1];

        el.setAttribute(
          "data-cy",
          `${componentName}.${vnode.elm.localName} ${vnode.elm.innerText}`
        );
      },
    },
  },
};

//onboarding.login-form.login-btn
//data-cy="login.login-form.login-btn"

//{view name}.{component name}.{element name}
